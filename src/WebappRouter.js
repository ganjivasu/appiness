// @flow
import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { connect } from 'react-redux';
import SearchUser  from "./user/components/SearchUser";
import UserEmployeeList from './user/components/UserEmployeeList';
import { Redirect } from 'react-router-dom';

type Props = {
	dispatch: *,
};


class WebappRouterContainer extends React.Component<Props, void> {

  render(): * {    
    return (
      <BrowserRouter>
        <Switch>
          <Route path='/home' component={SearchUser}/>
          <Route path='/:userId/employeeList' component={UserEmployeeList}/>
          <Route path="/*" render={(): * => <Redirect to={'/home'}/>}/>
        </Switch>
      </BrowserRouter>
    );
  }
}
const mapStateToProps = function(state: *): * {
  return {
    dispatch: state.dispatch,
  };
};

const WebappRouter = connect(mapStateToProps)(WebappRouterContainer);

export default WebappRouter;
