// @flow
import userDummyList from './dummyList'
const initialState = {
  employeeList: [],
};


function userEmployeeListReducers(state: * = initialState, action: *): * {
    switch(action.type) {
      case 'GET_USER_EMPLOYEE_LIST': {
        state = {...state, employeeList: userDummyList};
        break;
      } 
      default:
        break;
    }
  return state;
}

export default userEmployeeListReducers;
