// @flow
import userEmployeeListReducers from './userEmployeeListReducers';

const userSearchReducers =  {
  userEmployeeList: userEmployeeListReducers,
}
export default userSearchReducers;
