// @flow
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';

const styles = (): * => ({
  welcomeMessage: {
    textAlign: 'center'
  },
  title :{
    marginBottom: 30
  }
});

type Props = {
  classes: *,
  inProgress: boolean
};

class UserWelcomeMessage extends React.Component<Props,void> {
  render(): * {
    const {classes} = this.props;    
    return (
      <div className={classes.welcomeMessage}>
        <Typography variant={'h1'} color={'primary'} className={classes.title}>{'Welcome To Appiness '}</Typography>
        <Typography variant={'h4'} color={'primary'} className={classes.title}>{'Type User Name and Password and click Login'}</Typography> 
      </div>
    );
  }
}

export default withStyles(styles)(UserWelcomeMessage);
