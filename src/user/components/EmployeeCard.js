// @flow
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';

const styles = theme => ({
  grid:{
    padding: '10px',
  },
  contentStyle: {
    width: 180,
    fontWeight: 'bold'
  },
  details: {
    fontWeight: 400
  },
  eachDetails:{
    marginBottom: 25,
    marginTop: 25,
    clear: 'both',
    display: 'flex',
    alignItems: 'center',
    width: '100%'
  },
  nameId: {
    marginLeft: 20,
    marginTop: 35,
    overflow: 'hidden',
    cursor: "pointer",
    margin: '15px'
  },
});

type Props = {
  employee: *
};

class EmployeeCard extends React.Component<Props, void> {
  render(): * {
   
    const {classes, employee} = this.props;      
    return (
      <Grid item xs={12} sm={4} md={3} className={classes.grid}>
        <Paper>
          <div className={classes.nameId}>
            <div className={classes.eachDetails}>
              <Typography variant={'h4'} className={classes.contentStyle}>{'Id:'} </Typography> <Typography variant={'h4'} className={classes.details}>{employee.id}</Typography>
            </div>
            <div className={classes.eachDetails}>
              <Typography variant={'h4'} className={classes.contentStyle}>{'Name:'} </Typography> <Typography variant={'h4'} className={classes.details}>{employee.name}</Typography>
            </div>
            <div className={classes.eachDetails}>
              <Typography variant={'h4'} className={classes.contentStyle}>{'Age:'} </Typography> <Typography variant={'h4'} className={classes.details}>{employee.age}</Typography>
            </div>
            <div className={classes.eachDetails}>
              <Typography variant={'h4'} className={classes.contentStyle}>{'Gender:'} </Typography> <Typography variant={'h4'} className={classes.details}>{employee.gender}</Typography>
            </div>
            <div className={classes.eachDetails}>
              <Typography variant={'h4'} className={classes.contentStyle}>{'Email:'} </Typography> <Typography variant={'h4'} className={classes.details}>{employee.email}</Typography>
            </div>
            <div className={classes.eachDetails}>
              <Typography variant={'h4'} className={classes.contentStyle}>{'Phone Number:'} </Typography> <Typography variant={'h4'} className={classes.details}>{employee.phoneNo}</Typography>
            </div>   
            </div>
        </Paper>  
      </Grid>
    );
  }

}

export default withStyles(styles)(EmployeeCard);
