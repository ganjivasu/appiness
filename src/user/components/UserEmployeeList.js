// @flow
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import {connect} from 'react-redux';
import EmployeeCard from './EmployeeCard';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import { Redirect } from 'react-router-dom';

const styles = (theme): * => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    overflow: 'scroll',
    backgroundColor: theme.palette.background.paper,
    margin: '40px',
    height: '700px',
    marginTop: '-25px'
  },
  title :{
    marginBottom: 30
  },
  button: {
    marginLeft: '16px',
    marginTop: '45px',
    height: '45px',
    width: '90px',
    display: 'inline'
  },
});

type Props = {
  classes: *,
  dispatch: *,
  employeeList: *
};

type State = {
   redirectToHome: boolean
 }

class UserEmployeeList extends React.Component<Props,State> {

  state = {
    redirectToHome: false
  }

  onBackClick = (): * =>{
    this.setState({
      redirectToHome: true
    })
  }

  async componentDidMount(){
    const { dispatch } = this.props;
    dispatch({type: 'GET_USER_EMPLOYEE_LIST'})
   }


  renderEmployeeList = (): * =>{
    const {employeeList} = this.props;    
    const listGroup = employeeList.map((employee: *) => {
      return(
        <EmployeeCard key={employee.id} employee={employee}/>
      )
    });
    return listGroup;
  }

  render(): * {
    const {classes} = this.props;
    const {redirectToHome} = this.state;
    if(redirectToHome) return <Redirect to={'/Home'}/>  
    return (
      <Paper className={classes.root}>
        <Button variant="contained" color="secondary" className={classes.button} onClick={this.onBackClick}>
          Back
        </Button>
        {this.renderEmployeeList()}
      </Paper>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    dispatch: state.dispatch,
    employeeList: state.userEmployeeList.employeeList
  }
}

export default connect(mapStateToProps)(withStyles(styles)(UserEmployeeList));
