// @flow
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import {reduxForm} from 'redux-form';
import type {FormProps} from 'redux-form';
import UserInputTextField from '../../UserTextField/UserInputTextField'
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import {connect} from 'react-redux';
import { Redirect } from 'react-router-dom';
import {validate} from './LoginFormValidator';

const styles = (): * => ({
  root: {
    display: 'block',
    padding: '20px',
    clear: 'both'
  },
  button: {
    margin: '15px 0',
    padding: '8px 30px',
    borderRadius: 2,
    color: '#ffffff'
  },
  inputsWrapper: {
    display: 'inline-flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginBottom: 20,
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
    '& input': {
      padding: 10,
      backgroundColor: '#f1f3f4'
    }
  },
  inputField: {
    width: 350
  },
  actions: {
    width: '100%',
    textAlign: 'center',
    overflow: 'hidden',
    marginRight: '16px'
  },
  label: {
    marginRight: 20,
    fontWeight: 500,
    lineHeight: '120%'
  },
  textInput: {
    padding: 10
  }

});

type Props = {
  classes: *,
  dispatch: *,
  isEdit: *,
  handleClose: *,
  user: *,
} & FormProps;

type State = {
  redirectToList: boolean,
  loginUser: *
}

class LoginForm extends React.Component<Props, State> {

  state = {
    redirectToList: false,
    loginUser: ''
  }

  onSubmit = async(values: *): * => {
    this.setState({
      redirectToList: true,
      loginUser: values.username
    })
    
  }

  render(): * {
    const {classes, handleSubmit} = this.props;
    const {redirectToList, loginUser} = this.state;
    if(redirectToList) return <Redirect to={`/${loginUser}/employeeList`}/>
    
    return (
      <form onSubmit={handleSubmit(this.onSubmit)} className={classes.root}>
        <div className={classes.inputsWrapper}>
          <Typography variant={'h4'} className={classes.label}>{'Name:'}</Typography>
          <UserInputTextField
            name={'username'}
            id={'username'}
            type={'text'}
            label={'Username'}
            inputProps = {{className:classes.textInput}}
            fullWidth={false}
            className={classes.inputField}/>
        </div>
          <div className={classes.inputsWrapper}>
          <Typography variant={'h4'} className={classes.label}>{'Password:'}</Typography>
          <UserInputTextField
            name={'password'}
            id={'password'}
            type={'text'}
            label={'Password'}
            inputProps = {{className:classes.textInput}}
            fullWidth={false}
            className={classes.inputField}/>
        </div>
        <div className={classes.actions}>
          <Button
            type={'submit'}
            id={'update'}
            variant="contained"
            color="primary"
            className={classes.button}>
            {'login'}
          </Button>
        </div>
      </form>
    );
  }
}

const createReduxForm = reduxForm({ form: 'UserSearchListForm',
                                        validate
                                    });

const mapStateToProps = (state) => {
  return {
    dispatch: state.dispatch,
  }
}

export default connect(mapStateToProps)(withStyles(styles)(createReduxForm(LoginForm)));
