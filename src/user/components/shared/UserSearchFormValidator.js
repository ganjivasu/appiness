// @flow
import { validate as formValidator } from 'validate.js';
import * as R from 'ramda'

const constraints = {
  'username': {
    'presence': true,
    'format': {
      'pattern': /^[a-zA-Z0-9 ,@()&-/#]+$/,
      'message': "should contain only alphanumeric & (-,/#) characters."
    }
  },
}

export const validate = (values: *) => {
  const validationErrors = formValidator(values, constraints);
  if(validationErrors) {
    const fetchSingleError = (fieldsErrors) => fieldsErrors[0];
    const errors = R.map(fetchSingleError, validationErrors);
    return errors;
  }
  return {};
}
