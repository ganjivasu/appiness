// @flow
import { validate as formValidator } from 'validate.js';
import * as R from 'ramda'

const constraints = {
  'username': {
    'presence': true,
    'format': {
      'pattern': /^[hruday@gmail.com]+$/,
      'message': "Enter valid Email."
    }
  },
  'password': {
    'presence': true,
    'format': {
      'pattern': /^[hruday123]+$/,
      'message': "enter valid password"
    }
  },
}

export const validate = (values: *) => {
  const validationErrors = formValidator(values, constraints);
  if(validationErrors) {
    const fetchSingleError = (fieldsErrors) => fieldsErrors[0];
    const errors = R.map(fetchSingleError, validationErrors);
    return errors;
  }
  return {};
}
