// @flow
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import  LoginForm from './shared/LoginForm'
import UserWelcomeMessage from './UserWelcomeMessage';

const styles = (): * => ({
  welcomeMessage: {
    textAlign: 'center'
  },
  title :{
    marginBottom: 30
  }
});

type Props = {
  classes: *,
  inProgress: boolean
};

class SearchUser extends React.Component<Props,void> {
  render(): * {
    const {classes} = this.props;    
    return (
      <div className={classes.welcomeMessage}>
        <UserWelcomeMessage/>
        <LoginForm/>
      </div>
    );
  }
}

export default withStyles(styles)(SearchUser);
