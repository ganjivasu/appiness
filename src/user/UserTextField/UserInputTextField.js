// @flow
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import  UserBaseTextField from './UserBaseTextField';
import {Field} from 'redux-form';
import type {FieldProps} from 'redux-form';
import * as R from 'ramda';

const styles = (): * => ({

});

type Props = {
  id: string,
  name: string,
  label: string,
  startAdornment: *,
  fullWidth: boolean,
  type: string,
  classes: *
} & FieldProps;

const renderTextField = ({input, meta: {touched, error}, ...rest}: Props): React$Element<UserBaseTextField> => {
  return (
    <UserBaseTextField
			error={touched && !R.isNil(error)}
      errorText={touched && error}
			{...input}
			{...rest}/>
  );
};

class UserInputTextField extends React.Component<Props, void> {

  static defaultProps = {
    fullWidth: true,
    type:'text'
  };

  render(): * {
    const { name, ...rest} = this.props;
    return (
      <Field
				component={renderTextField}
				name={name}
				{...rest}/>
    );
  }
}

export default withStyles(styles)(UserInputTextField);
