// @flow
import React from 'react';
import {withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';

const styles = (): * => ({
});

type Props = {
  classes: *,
  name: string,
  label: string,
  id: string,
  type: string,
  position: string,
  variant: "outlined" | "standard" | "filled",
  startAdornment: *,
  fullWidth: boolean,
  errorText: string,
  value: string
};

class UserBaseTextField extends React.Component<Props,void> {

  static defaultProps = {
    variant: 'outlined',
    position: 'end'
  }

  render(): * {
    const {label, variant, id, name, type, value, position, startAdornment, fullWidth, errorText, ...rest} = this.props;
    return (
      <TextField
        id={id}
        placeholder={label}
        name={name}
        helperText={errorText}
        variant={variant}
        type={type}
        value={value}
        fullWidth={fullWidth}
        InputProps={(startAdornment) ? {
          startAdornment: (
            <InputAdornment position={position}>
               {startAdornment}
            </InputAdornment>
          ),
        }: null
      }
      {...rest}
      />
    );
  }
}

export default withStyles(styles)(UserBaseTextField);
