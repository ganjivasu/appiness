// @flow
import React from 'react';
import { Provider } from 'react-redux';
import { theme } from './theme';
import WebappRouter from './WebappRouter';
import { MuiThemeProvider } from '@material-ui/core/styles';
import store from './store';

type Props = {};

class App extends React.Component<Props, void> {
  render(): * {
    return (
      <MuiThemeProvider theme={theme}>
        <Provider store={store}>
          <WebappRouter/>
        </Provider>
      </MuiThemeProvider>
    );
  }
}

export default App;

