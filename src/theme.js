// @flow
import { createMuiTheme } from '@material-ui/core/styles';

export const theme = createMuiTheme({
  typography: {
    useNextVariants: true,
    fontFamily: [
      'Poppins',
      'Montserrat',
      'sans-serif',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(','),
    h1: {
      fontWeight: 500,
      fontSize: 32,
      letterSpacing: 0.1
    },
    h2: {
      fontWeight: 500,
      fontSize: 24,
      letterSpacing: 0.1
    },
    h3:{
      fontWeight: 500,
      fontSize: 20
    },
    h4:{
      fontWeight: 400,
      fontSize: 16,
      letterSpacing: 0.1
    },
    h5: {
      fontWeight: 400,
      fontSize: 15,
      letterSpacing: 0.1
    },
    h6: {
      fontSize: 14,
      fontWeight: 400,
      letterSpacing: 0.1
    },
    body1:{
      fontWeight: 400,
      fontSize: 16,
      letterSpacing: 0.1
    },
    body2:{
      fontWeight: 400,
      fontSize: 18,
      letterSpacing: 0.1
    }
  },
  palette: {
    primary: {main: '#00b5a5'},
    secondary: {main: '#00afc3'},
    textPrimary: {main: '#2a2b36'},
    textSecondary: {main: '#8e98af'},
    action: {main: '#9ba0a5'}
  },
});
