//@ flow
import {
  applyMiddleware,
  createStore,
  combineReducers
} from 'redux';
import logger from 'redux-logger';
import { reducer as formReducer } from 'redux-form';
import promiseMiddleware from 'redux-promise-middleware';
import userSearchReducers from './user/reducers/userSearchReducers';

const reduxFormReducer = {'form': formReducer};

const rootReducer = combineReducers({
  ...reduxFormReducer,
  ...userSearchReducers
});

const middleware = applyMiddleware(promiseMiddleware, logger);
const store = createStore(
  rootReducer,
  middleware
);

export default store;